using Currency.Core.Service;
using Currency.Core.SignalRApp;
using FluentValidation.AspNetCore;
using GlobalNews.Data.Data;
using GlobalNews.Data.Data.EntityFramework;
using GlobalNews.Infrastructure.Service;
using GlobalNews.Models.Models;
using GlobalNews.Shared.Abstract;
using GlobalNews.Shared.Interface;
using GlobalNews.Shared.Options;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace Currency.Core
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            string databaseConnectionString = Configuration.GetConnectionString("DefaultConnection");
            services.AddDbContext<ApplicationDbContext>(x => x.UseSqlServer(databaseConnectionString,
                x => x.MigrationsAssembly("NewsSite")));  //  correct?
            services.AddTransient<IMessageEmailService, EmailService>();

            services.AddTransient<IArticleRepository<Article>, ArticleRepository>();
            services.AddTransient<ICategoryRepository<Category>, CategoryRepository>();
            services.AddTransient<ICommentsRepository<Comment>, CommentsRepository>();
            services.AddTransient<DataManager>();

            services.AddSignalR();

            services.Configure<EmailOptions>(Configuration.GetSection(EmailOptions.EmailService));


            services.AddIdentity<User, IdentityRole>(options => {
                options.User.RequireUniqueEmail = true;
                options.Password.RequiredLength = 6;
                options.Password.RequireDigit = false;
                options.Password.RequireNonAlphanumeric = false;
                options.Password.RequireLowercase = false;
                options.Password.RequireUppercase = false;
            })
                .AddEntityFrameworkStores<ApplicationDbContext>().AddDefaultTokenProviders();


            services.AddAuthorization(x =>
            {
                x.AddPolicy(PolicyName.adminName, policy => { policy.RequireRole(RoleName.adminName); });
            });

            services.AddAuthorization(x =>
            {
                x.AddPolicy(PolicyName.moderatorName, policy => { policy.RequireRole(RoleName.moderatorName); });
            });

            services.AddControllersWithViews(x =>
            {
                x.Conventions.Add(new AdminAreaAuthorization(RoleName.adminName, PolicyName.adminName));
                x.Conventions.Add(new AdminAreaAuthorization(RoleName.moderatorName, PolicyName.moderatorName));
            }).AddFluentValidation();
            services.AddHostedService<InfoService>();
            services.AddMemoryCache();
            services.AddScoped<ArticleRightSidebarInterface, ArticleRightSidebarService>();
            services.AddHostedService<ModeratorService>();
            services.AddScoped<ModeratorSuspiciousArticlesInterface, ModeratorSuspiciousArticlesService>();
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }
            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseFileServer();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapHub<ChatHub>("/chat");

                endpoints.MapControllerRoute(
                    name: "admin",
                    pattern: "{area:exists}/{controller=Home}/{action=Index}/{id?}");

                endpoints.MapControllerRoute(
                    name: "moderator",
                    pattern: "{area:exists}/{controller=Home}/{action=Index}/{id?}");

                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");

                endpoints.MapDefaultControllerRoute();

            });
        }
    }
}
