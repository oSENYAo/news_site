﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GlobalNews.Shared.Interface
{
    public interface IMessageEmailService
    {
        Task SendEmailMessage(string email, string subject, string message);
    }
}
