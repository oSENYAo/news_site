﻿using Currency.Core.ViewModels;
using GlobalNews.Data.Data;
using GlobalNews.Models.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Currency.Core.ViewComponents
{
    public class HeaderViewComponent : ViewComponent
    {
        private readonly ApplicationDbContext context;
        public HeaderViewComponent(ApplicationDbContext context)
        {
            this.context = context;
        }
        public async Task<IViewComponentResult> InvokeAsync()
        {
            var result = await context.Users.FirstOrDefaultAsync(x => x.Email == User.Identity.Name);
            Author author = null;
            if (User.Identity.IsAuthenticated)
                author = await context.Authors
                   .FirstOrDefaultAsync(x => x.FirstName == result.FirstName && x.LastName == result.LastName);

            UserViewModel model = new UserViewModel();

            model.User = result;
            model.LinkAdmin = "admin/Roles";
            model.LinkModerator = "moderator/UserArticle";

            return View(model);
        }
    }
}
