﻿using GlobalNews.Data.Data;
using GlobalNews.Models.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Currency.Core.Areas.Moderator.Controllers
{
    [Area("Moderator")]
    [Authorize(Roles = "moderator")]
    public class SuspiciousArticlesController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly DataManager _dataManager;
        private readonly IMemoryCache _cache;

        public SuspiciousArticlesController(ApplicationDbContext context, DataManager dataManager, IMemoryCache cache)
        {
            _context = context;
            _dataManager = dataManager;
            _cache = cache;
        }
        public async Task<IActionResult> Index()
        {
            if (_cache.TryGetValue("key_article", out List<Article> articles))
            {

            }
            if (articles == null)
            {
                return Content("Всё хорошо");
            }
            return View(articles);
        }
    }
}
