﻿using GlobalNews.Data.Data;
using GlobalNews.Models.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Currency.Core.ViewComponents
{
    public class SidebarViewComponent : ViewComponent
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<User> _manager;

        public SidebarViewComponent(ApplicationDbContext context, UserManager<User> manager)
        {
            this._context = context;
            _manager = manager;
        }
        public async Task<IViewComponentResult> InvokeAsync()
        {
            if (User.Identity.IsAuthenticated)
            {
                User user = await _context.Users
                .Include(x => x.Categories)
                .FirstOrDefaultAsync(x => x.Id == HttpContext.User.FindFirst(ClaimTypes.NameIdentifier).Value);
                return View(user.Categories.OrderByDescending(x => x.TimeSubscription).ToList());
            }
            return View(await _context.Categories.ToListAsync());
        }
    }
}
