﻿using GlobalNews.Data.Data;
using GlobalNews.Models.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Currency.Core.Areas.Moderator.Controllers
{
    [Area("Moderator")]
    [Authorize(Roles = "moderator")]
    public class UserArticleController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly DataManager _dataManager;
        public UserArticleController(ApplicationDbContext context, DataManager dataManager)
        {
            _context = context;
            _dataManager = dataManager;
        }
        public async Task<IActionResult> Index()
        {
            var result = await _context.Articles.Where(x => x.ArticleStatus == ArticleStatus.Review).ToListAsync();
            if (result != null)
            {
                return View(result);
            }
            return Content("Статей для рецензии нет");
        }

        public async Task<IActionResult> UserArticleReview(int id)
        {
            var article = await _context.Articles.FirstOrDefaultAsync(x => x.Id == id);
            if (article != null)
            {
                return RedirectToAction("ReviewArticle", "Article", new { area = "", id = article.Id });
            }
            return Content("Статья не нуждается в рецензии");
        }

    }
}
