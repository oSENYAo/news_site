﻿using GlobalNews.Data.Data;
using GlobalNews.Models.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Currency.Core.Controllers
{
    public class AuthorController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<User> _userManager;

        public AuthorController(ApplicationDbContext context, UserManager<User> userManager)
        {
            _context = context;
            _userManager = userManager;
        }
        public async Task<IActionResult> Index()
        {
            var authors = await _context.Authors
                .Include(x => x.Articles.Where(x => x.ArticleStatus == ArticleStatus.Published))
                .ToListAsync();
            return View(authors);
        }

        public async Task<IActionResult> Author(int id)
        {
            var author = await _context.Authors
            .Include(x => x.Articles.Where(x => x.ArticleStatus == ArticleStatus.Published))
            .Include(x => x.Users)
            .FirstOrDefaultAsync(x => x.Id == id);
            return View(author);
        }

        [HttpPost]
        [Authorize]
        public async Task<IActionResult> Subscribe(int id)
        {
            var author = await _context.Authors
                .Include(x => x.Articles.Where(x => x.ArticleStatus == ArticleStatus.Published))
                .Include(x => x.Users)
                .FirstOrDefaultAsync(x => x.Id == id);
            var user = await _userManager.GetUserAsync(User);
            user.Authors.Add(author);
            _context.Users.Update(user);
            await _context.SaveChangesAsync();

            return View("Author", author);
        }

        [HttpPost]
        [Authorize]
        public async Task<IActionResult> Unsubscribe(int id)
        {
            var author = await _context.Authors
                .Include(x => x.Articles.Where(x => x.ArticleStatus == ArticleStatus.Published))
                .Include(x => x.Users)
                .FirstOrDefaultAsync(x => x.Id == id);
            var user = await _userManager.GetUserAsync(User);

            user.Authors.Remove(author);
            _context.Users.Update(user);
            await _context.SaveChangesAsync();

            return View("Author", author);
        }
    }
}
