﻿using GlobalNews.Models.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Currency.Core.ViewModels
{
    public class SubscribeViewModel
    {
        public List<Author> Authors { get; set; } = new List<Author>();
    }
}
