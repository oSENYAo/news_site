﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GlobalNews.Shared.Abstract
{
    public interface IArticleRepository<TEntity>
    {
        Task<IEnumerable<TEntity>> GetAllArticles();
        Task<TEntity> GetArticleById(int id);
        Task<IEnumerable<TEntity>> GetArticleByKeyWord(string keyWords);
        Task SaveArticle(TEntity article);
        Task DeleteArticleById(int id);
        Task<IEnumerable<TEntity>> ArticleRightSideBar();
        Task SaveDataAsync();
    }
}
