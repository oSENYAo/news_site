﻿using GlobalNews.Data.Data;
using GlobalNews.Models.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Currency.Core.Controllers
{
    public class RatingController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<User> _manager;

        public RatingController(ApplicationDbContext context, UserManager<User> manager)
        {
            _context = context;
            _manager = manager;
        }
        [Authorize]
        [HttpPost]
        public async Task<IActionResult> Rating(string btnAction, int articleId, int voteId)
        {
            var user = await _manager.GetUserAsync(User);
            var vote = await _context.Votes.FirstOrDefaultAsync(x => x.VoteId == voteId);
            var article = await _context.Articles.FirstOrDefaultAsync(x => x.Id == articleId);
            if (vote == null)
            {
                if (btnAction.ToLower() == "нравится")
                {
                    vote = new Vote
                    {
                        UserId = user.Id,
                        User = user,
                        VoteResult = VoteResult.Like,
                        Article = article,
                        ArticleId = article.Id
                    };
                    article.LikeRating += 1;
                    article.Rating = article.DislikeRating + article.LikeRating;
                }
                else if (btnAction.ToLower() == "не нравится")
                {
                    vote = new Vote
                    {
                        UserId = user.Id,
                        User = user,
                        VoteResult = VoteResult.DisLike,
                        Article = article,
                        ArticleId = article.Id
                    };
                    article.DislikeRating -= 1;
                    article.Rating = article.DislikeRating + article.LikeRating;
                }
                await _context.Votes.AddAsync(vote);
                await _context.SaveChangesAsync();
            }
            else if (vote != null)
            {
                if (btnAction.ToLower() == "нравится" && vote.VoteResult == VoteResult.DisLike)
                {
                    vote.VoteResult = VoteResult.Like;
                    article.Rating++;
                    _context.Votes.Update(vote);
                    await _context.SaveChangesAsync();
                    return RedirectToAction("Article", "Home", new { id = articleId });
                }
                else if (btnAction.ToLower() == "не нравится" && vote.VoteResult == VoteResult.Like)
                {
                    vote.VoteResult = VoteResult.DisLike;
                    article.Rating--;
                    _context.Votes.Update(vote);
                    await _context.SaveChangesAsync();
                    return RedirectToAction("Article", "Home", new { id = articleId });
                }
                return RedirectToAction("Article", "Home", new { id = articleId });
            }
            return RedirectToAction("Article", "Home", new { id = articleId });
        }
    }
}
