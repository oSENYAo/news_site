﻿using GlobalNews.Data.Data;
using GlobalNews.Models.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Currency.Core.Controllers
{
    [Authorize]
    public class ChatsController : Controller
    {
        private readonly ApplicationDbContext _context;

        public ChatsController(ApplicationDbContext context)
        {
            _context = context;
        }
        // Кнопка написать 
        public async Task<IActionResult> CreatePrivateRoom(string userId)
        {

            var chat = new Chat
            {
                Type = ChatType.Private,
            };
            var result = _context.Chats.Where(x => x.Authors.Any(x => x.UserAuthorId == userId))
                .FirstOrDefault(x => x.Authors.Any(x => x.UserAuthorId == User.FindFirst(ClaimTypes.NameIdentifier).Value));
            if (result != null)
            {
                return RedirectToAction("Chat", new { id = result.Id });
            }

            chat.Authors.Add(new ChatUser
            {
                AuthorId = "1",
                UserAuthorId = userId,
            });

            chat.Authors.Add(new ChatUser
            {
                AuthorId = "2",
                UserAuthorId = User.FindFirst(ClaimTypes.NameIdentifier).Value,
            });

            await _context.Chats.AddAsync(chat);

            await _context.SaveChangesAsync();
            return RedirectToAction("Chat", new { id = chat.Id });
        }

        [HttpGet]
        public async Task<IActionResult> Chat(int id)
        {
            var chat = await _context.Chats.Include(x => x.Messages).FirstOrDefaultAsync(x => x.Id == id);
            return View(chat);
        }
        [HttpPost]
        public async Task<IActionResult> CreateMessage(int chatId, string message)
        {
            var Message = new Message
            {
                ChatId = chatId,
                Text = message,
                Name = User.Identity.Name,
                Timestamp = DateTime.Now
            };

            await _context.Messages.AddAsync(Message);
            await _context.SaveChangesAsync();
            return RedirectToAction("Chat", new { id = chatId });
        }

        public async Task<IActionResult> Index()
        {

            var chats = await _context.Chats
                .Include(x => x.Authors)
                .ThenInclude(x => x.Author)
                .Where(x => x.Type == ChatType.Private && x.Authors.Any(y => y.UserAuthorId == User.FindFirst(ClaimTypes.NameIdentifier).Value))
                .ToListAsync();
            return View(chats);
        }
    }
}
