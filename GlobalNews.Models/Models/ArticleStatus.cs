﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GlobalNews.Models.Models
{
    public enum ArticleStatus
    {
        Saved,
        Review,
        Approved,
        Published,
    }
}
