﻿using GlobalNews.Models.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Currency.Core.Areas.Admin.ViewModels
{
    public class CategoryViewModels
    {
        public List<Category> Categories { get; set; } = new List<Category>();
        public string Name { get; set; }

    }
}
