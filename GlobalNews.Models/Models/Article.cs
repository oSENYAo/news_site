﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GlobalNews.Models.Models
{
    public class Article
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Text { get; set; }
        public int LikeRating { get; set; }
        public int DislikeRating { get; set; }
        public int Rating { get; set; }
        public ArticleStatus ArticleStatus { get; set; }
        public DateTime TimeCreatedArticle { get; set; }
        public DateTime TimeNonPublishedArticle { get; set; }
        public DateTime TimePublishedArticle { get; set; }
        public string KeyWord { get; set; }
        public Author Author { get; set; }
        public Category Category { get; set; }
        public Picture Picture { get; set; }
        public List<Vote> Votes { get; set; } = new List<Vote>();
        public List<Comment> Commentaries { get; set; } = new List<Comment>();
        public List<ReviewComment> reviewCommentaries { get; set; } = new List<ReviewComment>();
    }
}
