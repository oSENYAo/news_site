﻿using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Currency.Core.Service
{
    public class InfoService : BackgroundService
    {
        private readonly IMemoryCache _cache;
        private readonly IServiceProvider _serviceProvider;

        public InfoService(IMemoryCache cache, IServiceProvider serviceProvider)
        {
            _cache = cache;
            _serviceProvider = serviceProvider;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            while (!stoppingToken.IsCancellationRequested)
            {
                try
                {
                    using var scope = _serviceProvider.CreateScope();
                    var request = scope.ServiceProvider.GetRequiredService<ArticleRightSidebarInterface>();
                    var result = await request.ArticleRightSideBar();
                    _cache.Set("key", result, TimeSpan.FromDays(7));
                }
                catch (Exception)
                {
                    throw new Exception();
                }
                await Task.Delay(TimeSpan.FromDays(7), stoppingToken);
            }
        }
    }
}
