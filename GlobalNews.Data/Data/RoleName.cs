﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GlobalNews.Data.Data
{
    public static class RoleName
    {
        public const string adminName = "admin";
        public const string moderatorName = "moderator";
    }
}
