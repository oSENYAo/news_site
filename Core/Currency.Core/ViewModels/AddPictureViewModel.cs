﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Currency.Core.ViewModels
{
    public class AddPictureViewModel
    {
        public IFormFile Picture { get; set; }

    }
}
