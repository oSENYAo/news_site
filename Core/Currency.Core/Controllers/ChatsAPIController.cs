﻿using Currency.Core.SignalRApp;
using GlobalNews.Data.Data;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GlobalNews.Models.Models;
namespace Currency.Core.Controllers
{
    [Authorize]
    [Route("[controller]")]
    public class ChatsAPIController : Controller
    {
        private IHubContext<ChatHub> _chat;
        private readonly ApplicationDbContext _context;

        public ChatsAPIController(IHubContext<ChatHub> chat, ApplicationDbContext context)
        {
            _chat = chat;
            _context = context;
        }


        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////


        [HttpPost("[Action]")]
        public async Task<IActionResult> SendMessage(int roomId, string message, [FromServices] ApplicationDbContext _context)
        {
            var Message = new Message
            {
                ChatId = roomId,
                Text = message,
                Name = User.Identity.Name,
                Timestamp = DateTime.Now
            };

            await _context.Messages.AddAsync(Message);
            await _context.SaveChangesAsync();
            await _chat.Clients.Group(roomId.ToString()).SendAsync("ReceiveMessage"
                , new { Text = Message.Text, Name = Message.Name, Timestamp = Message.Timestamp.ToString("dd/mm/yyyy hh:mm:ss") });
            return Ok();
        }

        [HttpPost("[Action]/{connectionId}/{roomId}")]
        public async Task<IActionResult> JoinRoom(string connectionId, string roomId)
        {
            await _chat.Groups.AddToGroupAsync(connectionId, roomId);
            return Ok();
        }


    }
}