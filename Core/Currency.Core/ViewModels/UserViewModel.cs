﻿using GlobalNews.Models.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Currency.Core.ViewModels
{
    public class UserViewModel
    {
        public User User { get; set; }
        public int ArticleId { get; set; }
        public string LinkModerator { get; set; }
        public string LinkAdmin { get; set; }
        public string Keywords { get; set; }

    }
}
