﻿using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Currency.Core.Service
{
    public class ModeratorService : BackgroundService
    {
        private readonly IMemoryCache _cache;
        private readonly IServiceProvider _serviceProvider;

        public ModeratorService(IMemoryCache cache, IServiceProvider serviceProvider)
        {
            _cache = cache;
            _serviceProvider = serviceProvider;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            while (!stoppingToken.IsCancellationRequested)
            {
                try
                {
                    using var scope = _serviceProvider.CreateScope();
                    var request = scope.ServiceProvider.GetRequiredService<ModeratorSuspiciousArticlesInterface>();
                    var result = await request.SuspiciousArticles();

                    _cache.Set("key_article", result, TimeSpan.FromMinutes(1440)); // сколько будет показываться инфа на странице
                }
                catch (Exception)
                {
                    throw new Exception();
                }
                await Task.Delay(TimeSpan.FromMinutes(1440), stoppingToken); // Время обновления информации в БД и на странице 
            }
        }
    }
}
