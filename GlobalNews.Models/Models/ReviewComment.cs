﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GlobalNews.Models.Models
{
    public class ReviewComment
    {
        public int Id { get; set; }
        public string Text { get; set; }
        public DateTime TimeCreateReviewComment { get; set; }
        public User User { get; set; }
        public Article Article { get; set; }
    }
}
