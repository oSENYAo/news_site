﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Currency.Core.ViewModels
{
    public class ChangeProfileViewModel
    {
        public string Userid { get; set; }
        [Required]
        public string Email { get; set; }
        [Required]
        [MaxLength(25, ErrorMessage = "Макс длина 25")]
        [MinLength(4, ErrorMessage = "Мин длина 4")]
        public string FirstName { get; set; }

        [Required]
        [MaxLength(25, ErrorMessage = "Макс длина 25")]
        [MinLength(4, ErrorMessage = "Мин длина 4")]
        public string LastName { get; set; }

        //[Required]
        //[DataType(DataType.Password)]
        //public string Password { get; set; }

        //[Required]
        //[Compare("Password", ErrorMessage = "Пароли не совпадают!")]
        //[DataType(DataType.Password)]
        //public string PasswordConfirm { get; set; }
        public IFormFile Avatar { get; set; }
        public byte[] byteAvatar { get; set; }

    }
}
