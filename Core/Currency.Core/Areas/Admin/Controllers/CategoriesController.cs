﻿using Currency.Core.Areas.Admin.ViewModels;
using GlobalNews.Data.Data;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Currency.Core.Areas.Admin.Controllers
{
    [Area("admin")]
    [Authorize(Roles = "admin")]
    public class CategoriesController : Controller
    {
        private readonly DataManager dataManager;
        private readonly ApplicationDbContext _context;
        public CategoriesController(DataManager dataManager, ApplicationDbContext context)
        {
            this.dataManager = dataManager;
            _context = context;
        }
        public async Task<IActionResult> Index()
        {
            CategoryViewModels model = new CategoryViewModels();
            model.Categories = await _context.Categories.ToListAsync();
            return View(model);
        }
        public IActionResult Create() => View();

        [HttpPost]
        public async Task<IActionResult> Create(string name)
        {
            if (!string.IsNullOrEmpty(name))
            {
                var result = await dataManager.functionCategory.AddCategoryAsync(name);
                await dataManager.functionCategory.SaveDataAsync();
                if (result != null)
                {
                    return RedirectToAction("Index");
                }
            }
            return View(name);
        }

        [HttpPost]
        public async Task<IActionResult> Delete(string name)
        {
            if (!string.IsNullOrEmpty(name))
            {
                var result = await _context.Categories.FirstOrDefaultAsync(x => x.Name == name);
                if (result != null)
                {
                    _context.Categories.Remove(result);
                    await _context.SaveChangesAsync();
                    return RedirectToAction("Index");
                }

            }
            return Content("Статья не найдена");
        }
    }
}
