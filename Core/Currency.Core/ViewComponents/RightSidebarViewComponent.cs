﻿using GlobalNews.Data.Data;
using GlobalNews.Models.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Currency.Core.ViewComponents
{
    public class RightSidebarViewComponent : ViewComponent
    {
        private readonly ApplicationDbContext _context;
        private readonly IMemoryCache _cache;

        public RightSidebarViewComponent(ApplicationDbContext context, IMemoryCache cache)
        {
            _context = context;
            _cache = cache;
        }
        public async Task<IViewComponentResult> InvokeAsync()
        {
            if (_cache.TryGetValue("key", out List<Article> articles))
            {

            }
            if (articles == null)
            {
                articles = await _context.Articles
                   .Include(x => x.Author)
                   .Where(x => x.ArticleStatus == ArticleStatus.Published)
                   .OrderByDescending(x => x.Rating)
                   .Take(5)
                   .ToListAsync();
            }
            return View(articles);
        }
    }
}
