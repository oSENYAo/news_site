﻿using GlobalNews.Models.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Currency.Core.ViewModels
{
    public class SubscriptionViewModel
    {
        public List<List<Author>> Authors { get; set; } = new List<List<Author>>();
        public List<List<Category>> Categories { get; set; } = new List<List<Category>>();

    }
}
