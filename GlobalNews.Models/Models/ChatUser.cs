﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GlobalNews.Models.Models
{
    public class ChatUser
    {
        public string AuthorId { get; set; }
        public string UserAuthorId { get; set; }
        public Author Author { get; set; }
        public int ChatId { get; set; }
        public Chat Chat { get; set; }
        public UserRole UserRole { get; set; }
    }
}
