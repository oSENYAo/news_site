﻿using GlobalNews.Models.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Currency.Core.ViewModels
{
    public class ArticleViewModel
    {
        public Article Article { get; set; }
        [Display(Name = ("Введите текст комментария"))]
        public string CommentText { get; set; }
        public ReviewComment ReviewComment { get; set; }
        public bool moderatorArticle { get; set; }
        public bool IsApproved { get; set; }
        public string UserId { get; set; }

    }
}
