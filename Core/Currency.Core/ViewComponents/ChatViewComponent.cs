﻿using GlobalNews.Data.Data;
using GlobalNews.Models.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Currency.Core.ViewComponents
{
    public class ChatViewComponent : ViewComponent
    {
        private readonly ApplicationDbContext _context;

        public ChatViewComponent(ApplicationDbContext context)
        {
            _context = context;
        }
        public async Task<IViewComponentResult> InvokeAsync()
        {

            var chats = await _context.Chats
                .Include(x => x.Authors)
                .ThenInclude(x => x.Author)
                .Where(x => x.Type == ChatType.Private && x.Authors.Any(y => y.AuthorId == HttpContext.User.FindFirst(ClaimTypes.NameIdentifier).Value)) // ту должен быть авторЮзерId
                .ToListAsync();
            return View(chats);
        }
    }
}
