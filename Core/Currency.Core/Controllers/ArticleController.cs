﻿using Currency.Core.ViewModels;
using GlobalNews.Data.Data;
using GlobalNews.Infrastructure.Service;
using GlobalNews.Models.Models;
using GlobalNews.Shared.Options;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using SixLabors.ImageSharp;
using SixLabors.ImageSharp.Processing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Currency.Core.Controllers
{
    [Authorize]
    public class ArticleController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly DataManager _dataManager;
        private readonly IWebHostEnvironment _hostingEnvironment;
        private readonly UserManager<User> _userManager;
        private readonly IOptions<EmailOptions> _options;
        public ArticleController(ApplicationDbContext context
            , DataManager dataManager
            , IWebHostEnvironment hostingEnvironment
            , UserManager<User> userManager
            , IOptions<EmailOptions> options)
        {
            _options = options;
            _context = context;
            _dataManager = dataManager;
            _hostingEnvironment = hostingEnvironment;
            _userManager = userManager;
        }

        public IActionResult Create()
        {
            if (User.Identity.IsAuthenticated)
            {
                ViewBag.MyCategory = new SelectList(_context.Categories, "Id", "Name");
                return View(new CreateArticleViewModel());
            }
            return RedirectToAction("Login", "Account");
        }


        [HttpPost]
        public async Task<IActionResult> Create(string btnAction, CreateArticleViewModel model, IFormFile titleImageFile)
        {
            ViewBag.MyCategory = new SelectList(_context.Categories, "Id", "Name");

            if (ModelState.IsValid)
            {

                if (btnAction.ToLower() == "опубликовать")
                {
                    var category = await _context.Categories.FirstOrDefaultAsync(x => x.Id == model.CategoryId);
                    Article article = new Article();
                    article.Title = model.Title;
                    article.Text = model.Text;
                    article.KeyWord = model.KeyWord;
                    article.Category = category;
                    article.ArticleStatus = ArticleStatus.Review;
                    article.TimeCreatedArticle = DateTime.Now;
                    article.TimeNonPublishedArticle = DateTime.Now;

                    var user = await _userManager.GetUserAsync(User);

                    Author author = null;
                    author = await _context.Authors.Include(x => x.Articles)
                        .FirstOrDefaultAsync(x => x.FirstName == user.FirstName && x.LastName == user.LastName);

                    if (author == null)
                        author = new Author();

                    author.FirstName = user.FirstName;
                    author.LastName = user.LastName;
                    author.UserAuthorId = user.Id;

                    article.Author = author;
                    author.Articles.Add(article);
                    _context.Authors.Update(author);

                    await _context.Articles.AddAsync(article);
                    await _context.SaveChangesAsync();

                    return RedirectToAction("ReviewArticle", new { id = article.Id });
                }
                else if (btnAction.ToLower() == "сохранить черновик" || btnAction.ToLower() == "добавить изображение")
                {
                    Article article = new Article();
                    article.ArticleStatus = ArticleStatus.Saved;
                    var category = await _context.Categories.FirstOrDefaultAsync(x => x.Id == model.CategoryId);
                    article.Title = model.Title;
                    article.Text = model.Text;
                    article.KeyWord = model.KeyWord;
                    article.Category = category;
                    article.TimeCreatedArticle = DateTime.Now;
                    article.TimeNonPublishedArticle = DateTime.Now;

                    if (titleImageFile != null)
                    {
                        model.FullPath += titleImageFile.FileName;
                        model.Path += titleImageFile.FileName;
                        using var image = Image.Load(titleImageFile.OpenReadStream());
                        image.Mutate(x => x.Resize(256, 256));
                        await image.SaveAsJpegAsync(model.FullPath);

                        Picture picture = new Picture
                        {
                            Path = model.FullPath,
                            Name = titleImageFile.FileName,
                        };

                        await _context.Pictures.AddAsync(picture);
                        article.Picture = picture;
                    }
                    var user = await _userManager.GetUserAsync(User);
                    Author author = null;
                    author = await _context.Authors.Include(x => x.Articles)
                        .FirstOrDefaultAsync(x => x.FirstName == user.FirstName && x.LastName == user.LastName);
                    if (author == null)
                        author = new Author();
                    author.FirstName = user.FirstName;
                    author.LastName = user.LastName;
                    author.UserAuthorId = user.Id;
                    article.Author = author;
                    author.Articles.Add(article);
                    _context.Authors.Update(author);
                    await _context.Articles.AddAsync(article);
                    await _context.SaveChangesAsync();
                    model.ArticleId = article.Id;
                    model.isSavedArticle = true;
                    return View("Create", model);
                }
            }
            return View(new CreateArticleViewModel());
        }

        #region страница для ревью
        [HttpGet]
        public async Task<IActionResult> ReviewArticle(int id)
        {
            ArticleViewModel model = new ArticleViewModel();

            var user = await _userManager.GetUserAsync(User);

            var article = await _context.Articles
                .Include(x => x.Category)
                .Include(x => x.Author)
                .Include(x => x.Picture)
                .Include(x => x.reviewCommentaries)
                .FirstOrDefaultAsync(x => x.Id == id);
            if (article == null)
            {
                return RedirectToAction("Index", "Home");
            }
            var author = await _context.Authors
                .FirstOrDefaultAsync(x => x.UserAuthorId == user.Id && x.Articles.Contains(article));

            model.moderatorArticle = false;   // статья модератора
            model.IsApproved = true;          // статья одобрена

            if (User.IsInRole("moderator") && author != null)
            {
                model.moderatorArticle = true;
            }

            model.Article = article;
            if (article.ArticleStatus != ArticleStatus.Review)
            {
                model.IsApproved = false;
            }
            return View(model);
        }
        #endregion

        #region создания ревью комментария
        [HttpPost]
        public async Task<IActionResult> CreateComment(ArticleViewModel model)
        {
            if (ModelState.IsValid)
            {
                var article = await _context.Articles
                    .Include(x => x.Category)
                    .Include(x => x.reviewCommentaries)
                    .FirstOrDefaultAsync(x => x.Id == model.Article.Id);
                ReviewComment comment = new ReviewComment
                {
                    Text = model.CommentText,
                    User = await _userManager.GetUserAsync(User),
                    Article = article
                };
                _context.ReviewComments.Add(comment);
                await _context.SaveChangesAsync();
                model.Article = article;
                return View("ReviewArticle", model);
            }
            return RedirectToAction("Index", "Home");
        }
        #endregion

        [HttpPost]
        public async Task<IActionResult> ApproveArticle(Article _article)
        {
            if (ModelState.IsValid)
            {
                var article = await _context.Articles
                    .Include(x => x.Category)
                    .Include(x => x.reviewCommentaries)
                    .Include(x => x.Author)
                    .FirstOrDefaultAsync(x => x.Id == _article.Id);
                article.ArticleStatus = ArticleStatus.Approved;
                _context.Articles.Update(article);
                await _context.SaveChangesAsync();

                var callbackUrl = Url.Action(
                            "ReviewArticle",
                            "Article",
                            new { id = article.Id },
                            protocol: HttpContext.Request.Scheme);

                EmailService emailService = new EmailService(_options);
                var user = await _context.Users.FirstOrDefaultAsync(x => x.Id == article.Author.UserAuthorId);

                await emailService.SendEmailMessage(user.Email, "Статья одобрена",
                    $"Вашу статью одобрили. Можете опубликовать её по ссылке: <a href='{callbackUrl}'>ссылке</a>");

                ArticleViewModel model = new ArticleViewModel();
                model.Article = article;
                return View("ReviewArticle", model);
            }
            return RedirectToAction("Index", "Home");
        }

        [HttpPost]
        public async Task<IActionResult> PublishArticle(Article _article)
        {
            if (ModelState.IsValid)
            {
                var article = await _context.Articles
                    .Include(x => x.Category)
                    .Include(x => x.reviewCommentaries)
                    .FirstOrDefaultAsync(x => x.Id == _article.Id);
                article.ArticleStatus = ArticleStatus.Published;
                article.TimePublishedArticle = DateTime.Now;
                _context.Articles.Update(article);
                await _context.SaveChangesAsync();
                return View("Published");
            }
            return RedirectToAction("Index", "Home");
        }

        [HttpPost]
        public async Task<IActionResult> RemoveArticle(int id)
        {
            var article = await _context.Articles.Include(x => x.Picture).FirstOrDefaultAsync(x => x.Id == id);
            if (article != null)
            {
                if (article.Picture != null && article.Picture.Path != null)
                {
                    System.IO.File.Delete(article.Picture.Path);
                    _context.Pictures.Remove(article.Picture);
                }

                _context.Articles.Remove(article);
                await _context.SaveChangesAsync();
                return View("RemoveArticle");
            }
            return Content("Не получилось удалить статью, она не найдена");
        }

        [HttpGet]
        public async Task<IActionResult> EditArticle(int id)
        {
            var article = await _context.Articles.FirstOrDefaultAsync(x => x.Id == id);
            if (article != null)
            {
                ViewBag.MyCategory = new SelectList(_context.Categories, "Id", "Name");
                return View("EditArticle", article);
            }
            return Content("Не получилось обновить статью, не найдена");
        }

        [HttpPost]
        public async Task<IActionResult> EditArticle(int id, string text, string title, int categoryId)
        {
            var article = await _context.Articles.FirstOrDefaultAsync(x => x.Id == id);
            if (article != null)
            {
                var category = await _context.Categories.FirstOrDefaultAsync(x => x.Id == categoryId);
                ArticleViewModel model = new ArticleViewModel();
                article.Text = text;
                article.Title = title;
                article.TimeCreatedArticle = DateTime.Now;
                article.TimeNonPublishedArticle = DateTime.Now;

                if (category != null)
                    article.Category = category;
                else
                    article.Category = default;
                model.Article = article;
                _context.Articles.Update(article);
                await _context.SaveChangesAsync();
                return View("ReviewArticle", model);
            }
            return Content("Не получилось обновить статью, не найдена");
        }



        public async Task<IActionResult> LinkCreateArticle(int id)
        {
            var article = await _context.Articles
                .Include(x => x.Author)
                .Include(x => x.Category)
                .FirstOrDefaultAsync(x => x.Id == id);
            CreateArticleViewModel model = new CreateArticleViewModel();
            ViewBag.MyCategory = new SelectList(_context.Categories, "Id", "Name");
            model.ArticleId = id;
            model.CategoryId = article.Category.Id;
            model.isSavedArticle = true;
            model.Text = article.Text;
            model.Title = article.Title;
            return View("Create", model);
        }

        [HttpPost]
        public async Task<IActionResult> SendForReview(int id, string btnAction, CreateArticleViewModel model, IFormFile titleImageFile)
        {
            ViewBag.MyCategory = new SelectList(_context.Categories, "Id", "Name");
            if (btnAction.ToLower() == "опубликовать")
            {
                var article = await _context.Articles.FirstOrDefaultAsync(x => x.Id == id);
                var category = await _context.Categories.FirstOrDefaultAsync(x => x.Id == model.CategoryId);

                article.Title = model.Title;
                article.Text = model.Text;
                article.Category = category;
                article.ArticleStatus = ArticleStatus.Review;
                article.TimeCreatedArticle = DateTime.Now;
                article.TimeNonPublishedArticle = DateTime.Now;

                _context.Articles.Update(article);
                await _context.SaveChangesAsync();

                return RedirectToAction("ReviewArticle", new { id = id });
            }
            else if (btnAction.ToLower() == "сохранить черновик")
            {
                var article = await _context.Articles.FirstOrDefaultAsync(x => x.Id == id);
                article.ArticleStatus = ArticleStatus.Saved;
                var category = await _context.Categories.FirstOrDefaultAsync(x => x.Id == model.CategoryId);
                article.Title = model.Title;
                article.Text = model.Text;
                article.Category = category;
                article.TimeCreatedArticle = DateTime.Now;
                article.TimeNonPublishedArticle = DateTime.Now;

                _context.Articles.Update(article);
                await _context.SaveChangesAsync();
                model.ArticleId = id;
                model.isSavedArticle = true;
                return View("Create", model);
            }
            return RedirectToAction("Index", "Home");
        }
    }
}
