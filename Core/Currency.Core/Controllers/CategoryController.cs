﻿using GlobalNews.Data.Data;
using GlobalNews.Models.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Currency.Core.Controllers
{
    public class CategoryController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly DataManager _manager;
        private readonly UserManager<User> _userManager;

        public CategoryController(ApplicationDbContext context, DataManager manager, UserManager<User> userManager)
        {
            _context = context;
            _manager = manager;
            _userManager = userManager;
        }
        public async Task<IActionResult> Index()
        {
            var category = await _context.Categories
                .Include(x => x.Articles.Where(x => x.ArticleStatus == ArticleStatus.Published))
                .ToListAsync();
            return View(category);
        }

        public async Task<IActionResult> Category(int id)
        {
            var category = await _context.Categories
                .Include(x => x.Articles.Where(x => x.ArticleStatus == ArticleStatus.Published))
                .ThenInclude(x => x.Author)
                .FirstOrDefaultAsync(x => x.Id == id);
            if (category != null)
            {
                return View(category);
            }
            return Content("Не найдена категория");
        }

        [HttpPost]
        [Authorize]
        public async Task<IActionResult> Subscribe(int id)
        {
            var category = await _context.Categories
                .Include(x => x.Articles.Where(x => x.ArticleStatus == ArticleStatus.Published))
                .Include(x => x.Users)
                .FirstOrDefaultAsync(x => x.Id == id);
            category.TimeSubscription = DateTime.Now;
            var user = await _userManager.GetUserAsync(User);
            if (user.Categories.Contains(category))
                return View("Category", category);

            user.Categories.Add(category);
            _context.Users.Update(user);
            await _context.SaveChangesAsync();
            return View("Category", category);
        }

        [HttpPost]
        [Authorize]
        public async Task<IActionResult> Unsubscribe(int id)
        {
            var category = await _context.Categories
                .Include(x => x.Articles.Where(x => x.ArticleStatus == ArticleStatus.Published))
                .Include(x => x.Users)
                .FirstOrDefaultAsync(x => x.Id == id);
            var user = await _userManager.GetUserAsync(User);
            category.TimeSubscription = DateTime.MinValue;
            user.Categories.Remove(category);
            _context.Users.Update(user);
            await _context.SaveChangesAsync();

            return View("Category", category);
        }
    }
}
