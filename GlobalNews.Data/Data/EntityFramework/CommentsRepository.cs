﻿using GlobalNews.Models.Models;
using GlobalNews.Shared.Abstract;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GlobalNews.Data.Data.EntityFramework
{
    public class CommentsRepository : ICommentsRepository<Comment>
    {
        private readonly ApplicationDbContext _context;
        public CommentsRepository(ApplicationDbContext context)
        {
            this._context = context;
        }
        public async Task<Comment> AddCommentary(Comment commentary)
        {
            await _context.Commentaries.AddAsync(commentary);
            return commentary;
        }
        public async Task<Comment> AddCommentary(string text)
        {
            var commentary = await _context.Commentaries.LastOrDefaultAsync();
            commentary.Text = text;
            _context.Commentaries.Add(commentary);
            return commentary;
        }

        public async Task DeleteCommentary(int id)
        {
            var commentary = await _context.Commentaries.FirstOrDefaultAsync(x => x.Id == id);
            if (commentary != null)
            {
                _context.Commentaries.Remove(commentary);
            }
        }

        public async Task SaveDataAsync()
        {
            await _context.SaveChangesAsync();
        }
    }
}
