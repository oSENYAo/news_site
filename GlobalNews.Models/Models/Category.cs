﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GlobalNews.Models.Models
{
    public class Category
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime TimeSubscription { get; set; }
        public List<Article> Articles { get; set; } = new List<Article>();
        public List<User> Users { get; set; } = new List<User>();
    }
}
