﻿using GlobalNews.Models.Models;
using GlobalNews.Shared.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GlobalNews.Data.Data
{
    public class DataManager
    {
        public IArticleRepository<Article> functionArticle { get; set; }
        public ICategoryRepository<Category> functionCategory { get; set; }
        public ICommentsRepository<Comment> functionCommentary { get; set; }
        public ApplicationDbContext context { get; set; }

        public DataManager(IArticleRepository<Article> functionArticle, ICategoryRepository<Category> functionCategory, ICommentsRepository<Comment> functionCommentary, ApplicationDbContext context)
        {
            this.functionArticle = functionArticle;
            this.functionCategory = functionCategory;
            this.functionCommentary = functionCommentary;
            this.context = context;
        }
    }
}
