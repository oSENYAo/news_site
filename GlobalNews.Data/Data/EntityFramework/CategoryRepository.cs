﻿using GlobalNews.Models.Models;
using GlobalNews.Shared.Abstract;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GlobalNews.Data.Data.EntityFramework
{
    public class CategoryRepository : ICategoryRepository<Category>
    {
        private readonly ApplicationDbContext _context;
        public CategoryRepository(ApplicationDbContext context)
        {
            this._context = context;
        }
        public async Task<IEnumerable<Category>> GetAllCategoryAsync() => await _context.Categories.ToListAsync();
        public async Task<Category> AddCategoryAsync(Category category)
        {
            await _context.Categories.AddAsync(category);
            return category;
        }
        public async Task<Category> AddCategoryAsync(string name)
        {
            Category category;
            using (var transaction = _context.Database.BeginTransaction())
            {
                category = await _context.Categories.OrderBy(x => x.Id).LastOrDefaultAsync();
                category.Name = name;
                category.Id += 1;
                _context.Categories.Add(category);
                _context.Database.ExecuteSqlInterpolated($"SET IDENTITY_INSERT NewsSite.dbo.Categories ON;");
                await _context.SaveChangesAsync();
                _context.Database.ExecuteSqlInterpolated($"SET IDENTITY_INSERT NewsSite.dbo.Categories OFF;");
                transaction.Commit();
            }
            return category;
        }

        public async Task DeleteCategoryAsync(int id)
        {
            var result = await _context.Categories.FirstOrDefaultAsync(x => x.Id == id);
            if (result != null)
            {
                _context.Categories.Remove(result);
            }
        }

        public async Task<Category> RenameCategoryAsync(Category category, string name)
        {
            var _category = await _context.Categories.FirstOrDefaultAsync(x => x.Id == category.Id);
            if (_category != null)
            {
                _category.Name = name;
            }
            return _category;
        }

        public async Task SaveDataAsync()
        {
            await _context.SaveChangesAsync();
        }
    }
}
