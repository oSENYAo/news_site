﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GlobalNews.Shared.Abstract
{
    public interface ICommentsRepository<TEntity>
    {
        Task<TEntity> AddCommentary(TEntity commentary);
        Task<TEntity> AddCommentary(string text);

        Task DeleteCommentary(int id);
        Task SaveDataAsync();
    }
}
