﻿using GlobalNews.Models.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Currency.Core.ViewModels
{
    public class UserHomeViewModel
    {
        public User User { get; set; }
        public List<Article> PublishedArticles { get; set; } = new List<Article>();
        public List<Article> OnReview { get; set; } = new List<Article>();
        public List<Article> Approved { get; set; } = new List<Article>();
        public List<Article> SavedArticles { get; set; } = new List<Article>();

    }
}
