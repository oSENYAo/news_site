﻿using GlobalNews.Models.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GlobalNews.Data.Data
{
    public class ApplicationDbContext : IdentityDbContext<User>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {

        }
        public DbSet<Article> Articles { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<Comment> Commentaries { get; set; }
        public DbSet<Picture> Pictures { get; set; }
        public DbSet<ReviewComment> ReviewComments { get; set; }
        public DbSet<Author> Authors { get; set; }
        public DbSet<Chat> Chats { get; set; }
        public DbSet<Message> Messages { get; set; }
        public DbSet<ChatUser> ChatUsers { get; set; }
        public DbSet<Vote> Votes { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.Entity<ChatUser>()
                .HasKey(x => new { x.ChatId, x.AuthorId });

            builder.Entity<IdentityRole>().HasData(new IdentityRole
            {
                Id = "8ADD6S41-B43A-E9E-CA8A-812247ADE392",
                Name = "admin",
                NormalizedName = "ADMIN"
            });

            builder.Entity<User>().HasData(new User
            {
                Id = "8AFD6841-B23A-A9D3-CA97-C12BEA352992",
                UserName = "ademail@mail.ru",
                NormalizedUserName = "ADEMAIL@MAIL.RU",
                Email = "ademail@mail.ru", // change
                EmailConfirmed = true,
                PasswordHash = new PasswordHasher<User>().HashPassword(null, "AdPassword1"),
                SecurityStamp = String.Empty,
                FirstName = "admin",
                LastName = "mainAdmin"
            });

            builder.Entity<IdentityUserRole<string>>().HasData(new IdentityUserRole<string>
            {
                RoleId = "8ADD6S41-B43A-E9E-CA8A-812247ADE392",
                UserId = "8AFD6841-B23A-A9D3-CA97-C12BEA352992"
            });

            builder.Entity<IdentityRole>().HasData(new IdentityRole
            {
                Id = "999D6S41-BAFC-E9E-CA8A-812247ADE392",
                Name = "moderator",
                NormalizedName = "MODERATOR"
            });

            builder.Entity<User>().HasData(new User
            {
                Id = "BAFD6841-B23A-A9D3-C017-C12BEA352992",

                UserName = "modemail@mail.ru",
                NormalizedUserName = "MODEMAIL@MAIL.RU",
                Email = "modemail@mail.ru", // change
                EmailConfirmed = true,
                PasswordHash = new PasswordHasher<User>().HashPassword(null, "ModPassword1"),
                SecurityStamp = String.Empty,
                FirstName = "moder",
                LastName = "mainModer"
            });

            builder.Entity<IdentityUserRole<string>>().HasData(new IdentityUserRole<string>
            {
                RoleId = "999D6S41-BAFC-E9E-CA8A-812247ADE392",
                UserId = "BAFD6841-B23A-A9D3-C017-C12BEA352992"
            });

            builder.Entity<Category>().HasData(new Category
            {
                Id = 1,
                Name = "It"
            });
            builder.Entity<Category>().HasData(new Category
            {
                Id = 2,
                Name = "Спорт"
            });
            builder.Entity<Category>().HasData(new Category
            {
                Id = 3,
                Name = "Медицина"
            });
        }
    }
}
