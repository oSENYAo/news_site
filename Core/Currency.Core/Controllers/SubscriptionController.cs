﻿using Currency.Core.ViewModels;
using GlobalNews.Data.Data;
using GlobalNews.Models.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Currency.Core.Controllers
{
    [Authorize]
    public class SubscriptionController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly DataManager _manager;
        private readonly UserManager<User> _userManager;

        public SubscriptionController(ApplicationDbContext context, DataManager manager, UserManager<User> userManager)
        {
            _context = context;
            _manager = manager;
            _userManager = userManager;
        }
        public async Task<IActionResult> Index()
        {
            SubscriptionViewModel model = new SubscriptionViewModel();
            var user = await _userManager.GetUserAsync(User);
            model.Authors = await _context.Users
                .Where(x => x.Id == user.Id)
                .Select(x => x.Authors).ToListAsync();
            model.Categories = await _context.Users
                .Where(x => x.Id == user.Id)
                .Select(x => x.Categories).ToListAsync();
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> UnsubscribeCategory(int id)
        {
            var category = await _context.Categories
                .Include(x => x.Articles.Where(x => x.ArticleStatus == ArticleStatus.Published))
                .Include(x => x.Users)
                .FirstOrDefaultAsync(x => x.Id == id);
            var user = await _userManager.GetUserAsync(User);

            user.Categories.Remove(category);
            _context.Users.Update(user);
            await _context.SaveChangesAsync();

            SubscriptionViewModel model = new SubscriptionViewModel();

            model.Authors = await _context.Users
                .Where(x => x.Id == user.Id)
                .Select(x => x.Authors)
                .ToListAsync();
            model.Categories = await _context.Users
                .Where(x => x.Id == user.Id)
                .Select(x => x.Categories)
                .ToListAsync();
            return View("Index", model);
        }

        [HttpPost]
        public async Task<IActionResult> UnsubscribeAuthor(int id)
        {
            var author = await _context.Authors
                .Where(x => x.Articles.Any(x => x.ArticleStatus == ArticleStatus.Published))
                .Include(x => x.Articles)
                .Include(x => x.Users)
                .FirstOrDefaultAsync(x => x.Id == id);
            var user = await _userManager.GetUserAsync(User);

            user.Authors.Remove(author);
            _context.Users.Update(user);
            await _context.SaveChangesAsync();

            SubscriptionViewModel model = new SubscriptionViewModel();

            model.Authors = await _context.Users.Where(x => x.Id == user.Id).Select(x => x.Authors).ToListAsync();
            model.Categories = await _context.Users.Where(x => x.Id == user.Id).Select(x => x.Categories).ToListAsync();
            return View("Index", model);
        }
    }
}
