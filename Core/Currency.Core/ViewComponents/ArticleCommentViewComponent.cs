﻿using GlobalNews.Data.Data;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Currency.Core.ViewComponents
{
    public class ArticleCommentViewComponent : ViewComponent
    {
        private readonly ApplicationDbContext _context;
        public ArticleCommentViewComponent(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<IViewComponentResult> InvokeAsync()
        {
            var result = await _context.Commentaries.ToListAsync();
            return View(result);
        }
    }
}
