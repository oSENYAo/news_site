﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GlobalNews.Shared.Options
{
    public class EmailOptions
    {
        public const string EmailService = "EmailService";
        public string EmailMeesageFromName { get; set; }
        public string EmailMeesageFromEmail { get; set; }
        public string Email { get; set; }
        public string EmailParol { get; set; }
    }
}
