﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GlobalNews.Shared.Abstract
{
    public interface ICategoryRepository<TEntity>
    {
        Task<IEnumerable<TEntity>> GetAllCategoryAsync();
        Task<TEntity> AddCategoryAsync(TEntity category);
        Task<TEntity> AddCategoryAsync(string name);
        Task DeleteCategoryAsync(int id);
        Task<TEntity> RenameCategoryAsync(TEntity category, string name);
        Task SaveDataAsync();
    }
}
