﻿using GlobalNews.Data.Data;
using GlobalNews.Models.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Currency.Core.Service
{
    public class ArticleRightSidebarService : ArticleRightSidebarInterface
    {
        private readonly ApplicationDbContext _context;

        public ArticleRightSidebarService(ApplicationDbContext context)
        {
            _context = context;
        }
        public async Task<IEnumerable<Article>> ArticleRightSideBar()
        {
            var articles = await _context.Articles
                   .Include(x => x.Author)
                   .Where(x => x.ArticleStatus == ArticleStatus.Published)
                   .OrderByDescending(x => x.Rating)
                   .Take(5)
                   .ToListAsync();
            return articles;
        }
    }
}
