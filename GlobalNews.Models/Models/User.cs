﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GlobalNews.Models.Models
{
    public class User : IdentityUser
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public byte[] Avatar { get; set; }
        public List<Vote> Votes { get; set; } = new List<Vote>();
        public List<Author> Authors { get; set; } = new List<Author>();
        public List<Category> Categories { get; set; } = new List<Category>();
        public List<Comment> Commentaries { get; set; } = new List<Comment>();
        public List<ReviewComment> reviewCommentaries { get; set; } = new List<ReviewComment>();
    }
}
