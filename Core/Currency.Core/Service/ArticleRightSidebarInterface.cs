﻿using GlobalNews.Models.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Currency.Core.Service
{
    public interface ArticleRightSidebarInterface
    {
        Task<IEnumerable<Article>> ArticleRightSideBar();
    }
}
