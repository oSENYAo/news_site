﻿using GlobalNews.Data.Data;
using GlobalNews.Models.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Currency.Core.Service
{
    public class ModeratorSuspiciousArticlesService : ModeratorSuspiciousArticlesInterface
    {
        private readonly ApplicationDbContext _context;

        public ModeratorSuspiciousArticlesService(ApplicationDbContext context)
        {
            _context = context;
        }
        public async Task<IEnumerable<Article>> SuspiciousArticles()
        {
            var articles = await _context.Articles
                .Where(x => x.ArticleStatus == ArticleStatus.Approved || x.ArticleStatus == ArticleStatus.Saved)
                .Include(x => x.Author)
                .ToListAsync();

            foreach (var article in articles)
            {
                DateTime time = article.TimeNonPublishedArticle;
                article.TimeNonPublishedArticle = time.AddDays(1);
                _context.Articles.Update(article);
                if (article.TimeNonPublishedArticle - article.TimeCreatedArticle >= TimeSpan.FromDays(5))
                {
                    _context.Articles.Remove(article);
                }
            }

            await _context.SaveChangesAsync();

            return articles;
        }
    }
}
