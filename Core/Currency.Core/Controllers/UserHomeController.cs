﻿using Currency.Core.ViewModels;
using GlobalNews.Data.Data;
using GlobalNews.Models.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Currency.Core.Controllers
{
    [Authorize]
    public class UserHomeController : Controller
    {
        private readonly UserManager<User> _manager;
        private readonly ApplicationDbContext _context;

        public UserHomeController(UserManager<User> manager, ApplicationDbContext context)
        {
            _manager = manager;
            _context = context;
        }

        public async Task<IActionResult> Index()
        {
            UserHomeViewModel model = new UserHomeViewModel();
            var user = await _manager.GetUserAsync(User);
            var _user = await _context.Users
                    .Where(x => x.Id == user.Id)
                    .Include(a => a.Authors)
                    .FirstOrDefaultAsync(x => x.Id == user.Id);

            var author = await _context.Authors
                .Include(x => x.Articles)
                .ThenInclude(x => x.Category)
                .FirstOrDefaultAsync(x => x.UserAuthorId == user.Id);
            model.User = _user;
            if (author != null)
            {
                model.PublishedArticles = author.Articles
                .Where(x => x.ArticleStatus == ArticleStatus.Published)
                .ToList();
                model.OnReview = author.Articles
                    .Where(x => x.ArticleStatus == ArticleStatus.Review).ToList();
                model.Approved = author.Articles
                    .Where(x => x.ArticleStatus == ArticleStatus.Approved).ToList();
                model.SavedArticles = author.Articles
                                .Where(x => x.ArticleStatus == ArticleStatus.Saved).ToList();
            }

            return View(model);
        }



        public async Task<IActionResult> ChangeProfile(string id)
        {
            if (id != null)
            {
                var user = await _context.Users.FirstOrDefaultAsync(x => x.Id == id);
                ChangeProfileViewModel model = new ChangeProfileViewModel();
                model.Userid = user.Id;
                model.Email = user.Email;
                model.FirstName = user.FirstName;
                model.LastName = user.LastName;
                return View(model);
            }
            return Content("Пользователь не найден");
        }
        [HttpPost]
        public async Task<IActionResult> ChangeProfile(string id, ChangeProfileViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = await _context.Users.FirstOrDefaultAsync(x => x.Id == id);
                if (model.LastName != null)
                    user.LastName = model.LastName;
                if (model.FirstName != null)
                    user.FirstName = model.FirstName;
                if (model.Email != null)
                    user.Email = model.Email;
                if (model.byteAvatar != null)
                    user.Avatar = model.byteAvatar;
                await _manager.UpdateAsync(user);
                return RedirectToAction("ChangeProfile", new { id = id });
            }
            return Redirect("Повторите ввод. Непра");
        }
    }
}
