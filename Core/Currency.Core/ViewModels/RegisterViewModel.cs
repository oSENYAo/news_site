﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Currency.Core.ViewModels
{
    public class RegisterViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "email")]
        public string Email { get; set; }
        [Required]
        [MaxLength(25, ErrorMessage = "Макс длина 25")]
        [MinLength(4, ErrorMessage = "Мин длина 4")]
        [Display(Name = "Имя")]
        public string FirstName { get; set; }

        [Required]
        [MaxLength(25, ErrorMessage = "Макс длина 25")]
        [MinLength(4, ErrorMessage = "Мин длина 4")]
        [Display(Name = "Фамилия")]
        public string LastName { get; set; }
        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Пароль")]
        public string Password { get; set; }

        [Required]
        [Compare("Password", ErrorMessage = "Пароли не совпадают!")]
        [DataType(DataType.Password)]
        [Display(Name = "Повтори пароль")]
        public string PasswordConfirm { get; set; }
        public IFormFile Avatar { get; set; }
        public byte[] byteAvatar { get; set; }

    }
}
