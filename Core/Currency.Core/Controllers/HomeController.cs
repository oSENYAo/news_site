﻿using Currency.Core.ViewModels;
using GlobalNews.Data.Data;
using GlobalNews.Models.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace Currency.Core.Controllers
{
    public class HomeController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<User> _userManager;
        private readonly DataManager _dataManager;
        public HomeController(ApplicationDbContext db, DataManager dataManager, UserManager<User> userManager)
        {
            _context = db;
            _dataManager = dataManager;
            _userManager = userManager;
        }

        [HttpGet]
        public async Task<IActionResult> Index()
        {

            var user = await _userManager.GetUserAsync(User);
            if (User.Identity.IsAuthenticated)
            {
                SubscribeViewModel model = new SubscribeViewModel();
                var _user = await _context.Users
                    .Where(x => x.Id == user.Id)
                    .Include(a => a.Authors)
                    .Include(x => x.Categories)
                    .ThenInclude(art => art.Articles.Where(i => i.ArticleStatus == ArticleStatus.Published)
                                            .OrderByDescending(x => x.TimePublishedArticle))
                    .FirstOrDefaultAsync(x => x.Id == user.Id);

                model.Authors = _user.Authors;
                if (model.Authors.Count == 0)
                {
                    var article = await _context.Articles
                .Include(x => x.Category)
                .Include(x => x.Author)
                .Where(x => x.ArticleStatus == ArticleStatus.Published)
                .OrderByDescending(x => x.TimePublishedArticle)
                .ToListAsync();

                    return View("GuestIndex", article);
                }
                return View(model);

            }
            var articles = await _context.Articles
                .Include(x => x.Category)
                .Include(x => x.Author)
                .Where(x => x.ArticleStatus == ArticleStatus.Published)
                .OrderByDescending(x => x.TimePublishedArticle)
                .ToListAsync();

            return View("GuestIndex", articles);
        }
        [HttpGet]
        public async Task<IActionResult> Article(int id)
        {
            var article = await _context.Articles
                .Include(x => x.Category)
                .Include(x => x.Commentaries)
                .Include(x => x.Picture)
                .Include(x => x.Author)
                .Include(x => x.Votes)
                .FirstOrDefaultAsync(x => x.Id == id);
            var articleForReview = await _context.Articles.ToListAsync();
            if (article is not null)
            {
                ArticleViewModel model = new ArticleViewModel();
                if (User.Identity.IsAuthenticated)
                {
                    var user = await _userManager.GetUserAsync(User);
                    model.UserId = user.Id;
                }
                model.Article = article;
                return View(model);
            }
            return Content("Статья удалена");
        }

        [HttpPost]
        [Authorize]
        public async Task<IActionResult> CreateComment(ArticleViewModel model)
        {
            if (ModelState.IsValid)
            {
                var article = await _context.Articles
                    .Include(x => x.Commentaries)
                    .FirstOrDefaultAsync(x => x.Id == model.Article.Id);
                Comment comment = new Comment
                {
                    Text = model.CommentText,
                    User = await _userManager.GetUserAsync(User),
                    Article = article,
                    TimeCreateComment = DateTime.Now
                };
                _context.Commentaries.Add(comment);
                await _context.SaveChangesAsync();
                return RedirectToAction("Article", new { id = article.Id });
            }
            return RedirectToAction("Index", "Home");
        }
        public async Task<IActionResult> CategoryList(int id)
        {
            var category = await _context
                .Categories
                .Include(x => x.Articles.Where(x => x.ArticleStatus == ArticleStatus.Published))
                .ThenInclude(x => x.Author)
                .FirstOrDefaultAsync(x => x.Id == id);
            return View(category);
        }


        public async Task<IActionResult> ArticlesByKeyword(string keywords)
        {
            var articles = await _context.Articles
                .Include(x => x.Category)
                .Include(x => x.Author)
                .Where(x => x.ArticleStatus == ArticleStatus.Published)
                .ToListAsync();
            var result = new List<Article>();
            foreach (var article in articles)
            {
                if (article.KeyWord.Contains(keywords))
                {
                    result.Add(article);
                }
            }
            return View(result);
        }
    }
}
