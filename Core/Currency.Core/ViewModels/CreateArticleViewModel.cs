﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Currency.Core.ViewModels
{
    public class CreateArticleViewModel
    {
        [Required(ErrorMessage = ("Это поле обязательно для заполнения"))]
        [Display(Name = ("Введите название статьи"))]
        public string Title { get; set; }
        [Required(ErrorMessage = ("Это поле обязательно для заполнения"))]
        [Display(Name = ("Выберите категорию для своей статьи"))]
        public int CategoryId { get; set; }
        [Required(ErrorMessage = ("Это поле обязательно для заполнения"))]
        [Display(Name = ("Введите текст статьи"))]
        public string Text { get; set; }
        [Display(Name = ("Введите ключевые слова"))]
        [RegularExpression(@"^[a-zA-Zа-яёА-ЯЁ0-9]+( [a-zA-Zа-яёА-ЯЁ0-9]+)*$", ErrorMessage = "Слова должны разделяться только пробелом")]
        [Required(ErrorMessage = ("Это поле обязательно для заполнения"))]
        public string KeyWord { get; set; }
        public int ArticleId { get; set; }
        public bool isSavedArticle { get; set; } = false;
        public string FullPath { get; set; } = @"D:\repositivs\news_site-dev1\wwwroot\Files\";
        public string Path { get; set; } = "~/Files/";

    }
}
