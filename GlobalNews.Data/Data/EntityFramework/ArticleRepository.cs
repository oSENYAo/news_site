﻿using GlobalNews.Models.Models;
using GlobalNews.Shared.Abstract;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GlobalNews.Data.Data.EntityFramework
{
    public class ArticleRepository : IArticleRepository<Article>
    {
        private readonly ApplicationDbContext _context;
        public ArticleRepository(ApplicationDbContext context)
        {
            this._context = context;
        }

        public async Task<IEnumerable<Article>> GetAllArticles()
        {
            return await _context.Articles.ToListAsync();
        }

        public async Task<Article> GetArticleById(int id) => await _context.Articles.FirstOrDefaultAsync(x => x.Id == id);
        public Task<IEnumerable<Article>> GetArticleByKeyWord(string keyWords)
        {
            throw new Exception("in develop");
        }

        public async Task SaveArticle(Article article)
        {
            await _context.Articles.AddAsync(article);
        }
        public async Task DeleteArticleById(int id)
        {
            var articles = await _context.Articles.FirstOrDefaultAsync(x => x.Id == id);
            if (articles != null)
            {
                _context.Articles.Remove(articles);
            }
        }
        public async Task SaveDataAsync()
        {
            await _context.SaveChangesAsync();
        }

        public async Task<IEnumerable<Article>> ArticleRightSideBar()
        {
            var articles = await _context.Articles
                        .Include(x => x.Author)
                        .Where(x => x.ArticleStatus == ArticleStatus.Published)
                        .OrderByDescending(x => x.Rating)
                        .Take(5)
                        .ToListAsync();
            return articles;
        }
    }
}
