﻿using GlobalNews.Data.Data;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Currency.Core.ViewComponents
{
    public class ReviewViewComponent : ViewComponent
    {
        private readonly ApplicationDbContext _context;
        public ReviewViewComponent(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<IViewComponentResult> InvokeAsync(int id)
        {
            var article = await _context.Articles.FirstOrDefaultAsync(x => x.Id == id);
            var result = article.reviewCommentaries;
            return View(result);
        }
    }
}
