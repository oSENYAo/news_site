﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GlobalNews.Models.Models
{
    public class Author
    {
        public int Id { get; set; }
        public string UserAuthorId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public List<ChatUser> ChatUsers { get; set; } = new List<ChatUser>();
        public List<Article> Articles { get; set; } = new List<Article>();
        public List<User> Users { get; set; } = new List<User>();
    }
}
